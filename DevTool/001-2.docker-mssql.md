Docker 安裝 MSSQL 
======


Micorsoft Docs
------
+ [部署和連線到 SQL Server Docker 容器](https://docs.microsoft.com/zh-tw/sql/linux/sql-server-linux-docker-container-deployment?view=sql-server-ver15&pivots=cs1-bash)

+ [快速入門：使用 Docker 執行 SQL Server 容器映像](https://docs.microsoft.com/zh-tw/sql/linux/quickstart-install-connect-docker?view=sql-server-2017&preserve-view=true&pivots=cs1-bash)

<br>

Docker Hub
------
<https://hub.docker.com/_/microsoft-mssql-server>

<br>

Docker 映像
------
### 拉取 ms-sql 映像

    docker pull mcr.microsoft.com/mssql/server:2017-latest

### 檢查映像

    docker images


### 檢查 ms-sql 映像

    docker images | grep mssql

Output:
```
REPOSITORY                       TAG           IMAGE ID       CREATED        SIZE
mcr.microsoft.com/mssql/server   2017-latest   5af364e169e4   2 months ago   1.37GB
```

### 刪除映像

    docker rmi ${IMAGE ID}
    docker rmi 5af364e169e4

<br>

Docker 容器
------
### 運行 ms-sql 容器

    docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Dev127336' \
    -p 1433:1433 --name sql1 \
    -d mcr.microsoft.com/mssql/server:2017-latest

**參數功能：**

+ run : docker 建立 container 並且執行的指令
+ -e 'ACCEPT_EULA=Y : 將 ACCEPT_EULA 設定為任何值，接受 End-User 授權合約。此為 SQL Server 映像的必要設定。
+ -e SA_PASSWORD=Dev127336 : 初始化 SA 用戶的密碼為 Dev127336。
+ -p 1433:1433 : 將容器的 1433 端口映射到主機的 1433 端口。
+ --name : 指定容器為 sql1
+ -d mcr.microsoft.com/mssql/server:2017-latest : 背景執行 MSQL 映像

### 檢查容器 (運行中)

    docker ps

Output:
```
CONTAINER ID   IMAGE                                        COMMAND                  CREATED          STATUS          PORTS                                       NAMES
a2e1d0c9cf5d   mcr.microsoft.com/mssql/server:2017-latest   "/opt/mssql/bin/nonr…"   16 minutes ago   Up 16 minutes   0.0.0.0:1433->1433/tcp, :::1433->1433/tcp   sql1
```

### 停止容器

    docker stop sql1

### 檢查容器 (全部)

    docker ps -a

Output:
```
CONTAINER ID   IMAGE                                        COMMAND                  CREATED          STATUS                        PORTS         NAMES
a2e1d0c9cf5d   mcr.microsoft.com/mssql/server:2017-latest   "/opt/mssql/bin/nonr…"   17 minutes ago   Exited (0) 3 seconds ago                    sql1 
```

### 啟動容器

    docker start sql1

### 進入容器

    docker exec -it sql1 bash

### 退出容器

    exit

### 刪除容器

    docker rm sql1

<br>

進入容器
------
`進入到 Linux 的操作環境`

### 修改 Root 密碼

```sql
docker exec -it sql1 /opt/mssql-tools/bin/sqlcmd \
   -S localhost -U SA -P 'Dev127336' \
   -Q 'ALTER LOGIN SA WITH PASSWORD="Dev127336"'
```

+ 之前如果未設定正確，可以再用 SQL 修改。

### 進入容器

    docker exec -it sql1 bash

### 添加遠程登錄用戶
```sql
/opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'Dev127336'
```

<br>

### SQL 測試
**資料庫**

```sql
create database DevDb; -- 創建資料庫
SELECT Name from sys.Databases; -- 查詢資料庫
GO

USE DevDb; -- 使用資料庫
GO
```

**資料表**

創建: 
```sql
create table app_info ( id int identity(1,1) NOT NULL, name nvarchar(50),version nvarchar(30), author nvarchar(50), remark nvarchar(100) primary key (id) );
GO
```

資料:
```sql
insert into app_info(name,version,author,remark) values ('JavaProjSE-v1.0.3','1.0.3','Enoxs','Java Project Simple Example - Version 1.0.3');
insert into app_info(name,version,author,remark) values ('JUnitSE','1.0.2','Enoxs', 'Java Unit Test Simple Example');
insert into app_info(name,version,author,remark) values ('SpringMVC-SE','1.0.2','Enoxs','Java Web Application Spring MVC - Simple Example（MyBatis）');
GO
```

查詢:
```sql
select * from app_info;
GO
```

<br>

### 離開 ms-sql

    quit

### 退出容器

    exit


<br>

Dbeaver
------
+ Host : localhost
+ Port : 1433
+ Database : DevDb
+ Authentication : SQL Server Authenticaltion
+ User name : SA
+ Password : Dev127336


### MS-SQL Driver
<https://docs.microsoft.com/zh-tw/sql/connect/jdbc/download-microsoft-jdbc-driver-for-sql-server?view=sql-server-ver15>











